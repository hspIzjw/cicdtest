#!/bin/bash

# Version
VERSION=${CI_COMMIT_REF_NAME}-${CI_PIPELINE_ID}   #前变量的值表示了正在构建的项目的分支或者标签名   流水线id
echo -e "Version: $VERSION"

DOCKER_REGISTRY='docker-registry.qiyi.virtual'
GROUP='lego-middle-end'
CONTAINER_PATH='cloud-fe'

echo "Begin to rm -rf package-lock.json"
#将package-lock.json删除，下面重新安装一遍最新版本，生成新的package-lock.json 版本冲突问题
rm -rf package-lock.json
echo "Begin to install node modules..." #开始下载node module是
npm install --registry http://jfrog.cloud.qiyi.domain:80/api/npm/npm/  #指向仓库 并下载
echo "Begin to rebuild node-sass"  #rebuild 更改包内容后进行重建 moduleName  解决更新node版本后与node-sass冲突问题
npm rebuild node-sass
echo "Begin to build..."  #build
npm run build
echo "Begin to build docker..."
#注意最后有个点，代表使用当前路径的 Dockerfile 进行构建 -t后面是新构建的镜像的名字 版本为$version  可用docker image查看是否构建成功
docker build -t $DOCKER_REGISTRY/$GROUP/$CONTAINER_PATH:$VERSION .   
#将本地的镜像上传到镜像仓库,要先登陆到镜像仓库 
docker push $DOCKER_REGISTRY/$GROUP/$CONTAINER_PATH:$VERSION
